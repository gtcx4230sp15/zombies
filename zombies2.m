% A second model, partly inspired by assumptions by Smith? (2009)
%
% H(t) = # of humans
% Z(t) = # of zombies
% D(t) = # of dead who might rise in the future
%
% Model -- Note: Y' == d/dt Y(t)
%
%   H' = (b - d)*H - g*H*Z
%   Z' = r*D - a*H*Z - x*Z
%   D' = d*H + g*H*Z - r*D
%
% ==> H' + Z' + D' = b*H - a*H*Z - x*Z
%
% Interpretation
%   (b - d)*H = Net human natural births
%       g*H*Z = Humans who die from zombie encounters
%       a*H*Z = Zombies whom humans kill
%         r*D = Dead who rise as zombies
%         c*Z = Zombies who die from decay

x0 = [ 500 ; 1 ; 0 ] % initial population of:
                     % [humans ; zombies ; resurrectible dead]

beta = 0 % 0.0001   % human's natural birth rate
delta = beta/2  % human's natural death rate

rho = 0.9 % zombie-gain rate
gamma = 0.01 % human-loss rate when interacting with zombies
alpha = 0.75*gamma % zombie-kill rate
chi = 0 % delta/10 % zombie decay rate


F = @(t, x) [ (beta - delta)*x(1) - gamma*x(1)*x(2)            ; ...
                         rho*x(3) - alpha*x(1)*x(2) - chi*x(2) ; ...
                       delta*x(1) + gamma*x(1)*x(2) - rho*x(3) ];

J = @(x) [ beta - delta - gamma*x(2),         -gamma*x(1),      0   ; ...
                         -alpha*x(2),   -alpha*x(1) - chi,    rho   ; ...
                               delta,         -gamma*x(1),   -rho   ];
           
[T, X] = ode45 (F, [0 50], x0);

X_last = X(length (X), :)'
Eig_J = eig (J (X_last))

H = X(:, 1);
Z = X(:, 2);
D = X(:, 3);

figure (1);
plot (T, H, '-', T, Z, 'r.', T, D, 'm-.');
xlabel ('Time');
ylabel ('Population');
legend ('Humans', 'Zombies', 'Dead...for now!');
grid on;

% eof
