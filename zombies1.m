% A first (naive) model: Lotka-Volterra model

beta = .01 % human birth rate
gamma = 1e-4 % human-loss rate when interacting with zombies
rho = gamma/2 % zombie-gain rate
alpha = .01 % zombie death rate
x0 = [ 500 ; 1 ] % initial population: [humans ; zombies]


F = @(t, x) [ beta*x(1) - gamma*x(1)*x(2) ; rho*x(1)*x(2) - alpha*x(2) ...
            ];

J = @(x) [(beta - gamma*x(2)) (-gamma) ; rho*x(2) (rho*x(1) - alpha)];

[T, X] = ode45 (F, [0 2500], x0);

H = X(:,1);
Z = X(:,2);

figure (1);
plot (T, H, 'o-', T, Z, 'rx:');
xlabel ('Time');
ylabel ('Population');
legend ('Humans', 'Zombies');
grid on;

% eof
