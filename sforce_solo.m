% "Social force" concept for swarming
%
% There are two populations, H and Z.
% The Z population remains static.
% The H population is repulsed by Z.

% Initial populations
h_total = 1;
z_total = .01;

L = 1;
n = 250;
dx = L / (n+1);

J = (1:n+2)'; % full index set
Ji = (2:n+1)'; % inner index set
Ji_p1 = Ji + 1; % shifted inner index set
Ji_m1 = Ji - 1; % shifted inner index set
u = ones (n+2, 1);

% Define the kernel function
c1 = 1;
c2 = 1;
K = @(R) c1 * sign (R) * exp (-c2 * abs (R));

% Initial human neighborhood
l_h = L / 10;
ih_left = floor ((L/3 - l_h/2) / dx);
ih_right = ceil ((L/3 + l_h/2) / dx);
H0 = zeros (n+2, 1);
H0(ih_left:ih_right) = h_total / l_h;

% Initial zombie neighborhood
l_z = L / 10;
iz_left = floor ((L/2 - l_z/2) / dx);
iz_right = ceil ((L/2 + l_z/2) / dx);
Z0 = zeros (n+2, 1);
Z0(iz_left:iz_right) = z_total / l_z;

% Compute convolution distance matrix
x = Ji*dx;
x_p1 = Ji_p1*dx;
x_m1 = Ji_m1*dx;
K_eval = K (x*u(Ji)' - u(Ji)*x');

% Some shifted variants of the above, for experimenting with
% different finite difference-based derivative approximations
K_eval_p1 = K(x_p1*u(Ji_p1)' - u(Ji_p1)*x_p1');
K_eval_m1 = K(x_m1*u(Ji_m1)' - u(Ji_m1)*x_m1');
K_eval_dx = K_eval_p1 - K_eval;
K_eval_2dx = K_eval_p1 - K_eval_m1;

% Function to integrate
Ji_H = Ji;
Ji_p1_H = Ji_p1;
Ji_m1_H = Ji_m1;
Ji_Z = n+2 + Ji;
Ji_p1_Z = n+2 + Ji_p1;
Ji_m1_Z = n+2 + Ji_m1;

F_int = @(t, y) ...
        -[0 ;
         (diag (y(Ji_p1_H) - y(Ji_H)) * (K_eval * (y(Ji_Z)))) ...
         + (diag (y(Ji_H)) * (K_eval_dx * y(Ji_Z))) ; ...
         0 ; ...
         zeros(n+2, 1) ; ...
         ];

%============================================================
% Run ODE solver

Y0 = [ H0 ; Z0 ];
[T, Y] = ode45 (F_int, [0 0.5], Y0);
H = Y(:, 1:n+2);
Z = Y(:, n+3:2*(n+2));

%============================================================
% Plots

% Plot initial conditions
figure (1); clf;
plot (J*dx, H0, '-', J*dx, Z0, '-.');
grid on;
legend ('Humans, H_0(x)', 'Zombies, Z_0(x)', -1);
title ('Initial population densities');
xlabel ('Position (x)');

% Plot solution over time
figure (2); clf;
pcolor (J*dx, T, H);
shading ('interp');
xlabel ('Position (x)');
ylabel ('Time (t)');
title ('H(x, t): Human population density');
axis square;
colorbar;

figure (3); clf;
pcolor (J*dx, T, Z);
shading ('interp');
shading ('interp');
xlabel ('Position (x)');
ylabel ('Time (t)');
title ('Z(x, t): Zombie population density');
axis square;
colorbar;

% Check conservation of population
figure (4); clf;
plot (T, sum (H, 2)*dx, '-', T, sum (Z, 2)*dx, '-.');
title ('Total populations over time');
xlabel ('Time (t)');
l_id = legend ('Humans, $\sum_j H(x_j \Delta x,t)$' ...
               , ['Zombies, $\sum_j Z(x_j,t) \Delta x$']);
set (l_id, 'Interpreter', 'Latex');
grid on;

% eof
