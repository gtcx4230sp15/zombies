# Zombies vs. Humans: Continuous population models #

A demo inspired by Smith? et al. (2009?) humans vs. zombies population model + Mogilner & Edelstein-Keshet (1999) swarming model.

Files:

* `zombies1.m`: Demonstrates a Lotka-Volterra-style model that exhibits cyclic population behavior.
* `zombies2.m`: Demonstrates a Smith?-style (infectious diseases) model.
* `sforce_solo.m`: Demonstrates a non-local swarming (or "aggregation") model: humans move away from zombies whose position is fixed in space.
* `sforce_pair.m`: Same as above, but both humans and zombies move: humans are repulsed by zombies but zombies are attracted to humans.
* `zombies3.m`: I forgot what this demo does and I am too lazy to do a `diff` to find out. Anyway, it's a file that is probably safe to ignore.
* `zombies4.m`: Combines `zombies2` and `sforce_pair`, that is, infectious-disease population dynamics plus swarming (aggregation).