% Humans vs. Zombies model with "swarming" force.

% Initial populations
h_total = .1 % total human population
z_total = h_total / 4; % zombies are a fraction of the total

% Human-zombie interactions
beta = 0 % 0.0001   % human's natural birth rate
delta = beta/2  % human's natural death rate

rho = 1.0 % zombie-gain rate
gamma = 0.5 % human-loss rate when interacting with zombies
alpha = gamma % zombie-kill rate
chi = 0 % delta/10 % zombie decay rate

% "Social-sense" rates
c_h1 = 1 % human speed
c_h2 = 1 % (inverse) strength of human "zombie-dar"
c_z1 = c_h1 / 3 % zombies are slower than humans ...
c_z2 = c_h2 / 100 % ... but have a good sense of flesh

% Define the spatial domain (discretized)
L = 1
n = 250;
dx = L / (n+1)

% Define the time domain
T_range = [0, 0.25]

J = (1:n+2)'; % full index set
Ji = (2:n+1)'; % inner index set
Ji_p1 = Ji + 1; % shifted inner index set
Ji_m1 = Ji - 1; % shifted inner index set
u = ones (n+2, 1);

% Define the kernel function
K_h = @(R) c_h1 * sign (R) * exp (-c_h2 * abs (R));

K_z = @(R) c_z1 * sign (R) * exp (-c_z2 * abs (R));

% Initial human neighborhood
l_h_tmp = L / 10;
ih_left = floor ((L/3 - l_h_tmp/2) / dx);
ih_right = ceil ((L/3 + l_h_tmp/2) / dx);
l_h = (ih_right - ih_left + 1) * dx
H0 = zeros (n+2, 1);
H0(ih_left:ih_right) = h_total / l_h;

% Initial zombie neighborhood
l_z_tmp = L / 10;
iz_left = floor ((L/2 - l_z_tmp/2) / dx);
iz_right = ceil ((L/2 + l_z_tmp/2) / dx);
l_z = (iz_right - iz_left + 1) * dx;
Z0 = zeros (n+2, 1);
Z0(iz_left:iz_right) = z_total / l_z;

% Initial pool of dead people
D0 = zeros (n+2, 1);

% Compute convolution distance matrices
x = Ji*dx;
x_p1 = Ji_p1*dx;
x_m1 = Ji_m1*dx;

K_hz_eval = K_h (x*u(Ji)' - u(Ji)*x');
K_hz_eval_p1 = K_h (x_p1*u(Ji_p1)' - u(Ji_p1)*x_p1');
K_hz_eval_m1 = K_h (x_m1*u(Ji_m1)' - u(Ji_m1)*x_m1');
K_hz_eval_dx = K_hz_eval_p1 - K_hz_eval;
K_hz_eval_2dx = K_hz_eval_p1 - K_hz_eval_m1;

K_zh_eval = K_z (x*u(Ji)' - u(Ji)*x');
K_zh_eval_p1 = K_z (x_p1*u(Ji_p1)' - u(Ji_p1)*x_p1');
K_zh_eval_m1 = K_z (x_m1*u(Ji_m1)' - u(Ji_m1)*x_m1');
K_zh_eval_dx = K_zh_eval_p1 - K_zh_eval;
K_zh_eval_2dx = K_zh_eval_p1 - K_zh_eval_m1;

% Index sets for packed vectors [H(1:n+2) ; Z(1:n+2) ; D(1:n+2)]
Ji_H = Ji;
Ji_p1_H = Ji_p1;
Ji_m1_H = Ji_m1;
Ji_Z = n+2 + Ji;
Ji_p1_Z = n+2 + Ji_p1;
Ji_m1_Z = n+2 + Ji_m1;
Ji_D = 2*(n+2) + Ji;

% Force exerted on humans due to zombies
F_h = @(t, y) ...
      -[
% dH / dt =
          0 ;
          (diag (y(Ji_p1_H) - y(Ji_H)) * (K_hz_eval * (y(Ji_Z)))) ...
          + (diag (y(Ji_H)) * (K_hz_eval_dx * y(Ji_Z))) ; ...
          0 ; ...
% dZ / dt =
          zeros(n+2, 1) ; ...
% dD / dt =
          zeros(n+2, 1) ; ...
       ];

% Force exerted on zombies due to humans
F_z = @(t, y) ...
      -[ ...
% dH / dt =
          zeros(n+2, 1) ; ...
% dZ / dt =
          0 ;
          (diag (y(Ji_p1_Z) - y(Ji_Z)) * (K_zh_eval' * (y(Ji_H)))) ...
          + (diag (y(Ji_Z)) * (K_zh_eval_dx' * y(Ji_H))) ; ...
          0 ; ...
% dD / dt =
          zeros(n+2, 1) ; ...
       ];

% Interaction forces
F_interact = @(t, y) ...
    [
        0 ; ...
        (beta - delta)*y(Ji_H) - gamma*y(Ji_H).*y(Ji_Z) ; ...
        0 ; ...
        0 ; ...
        rho*y(Ji_D) - alpha*y(Ji_H).*y(Ji_Z) - chi*y(Ji_Z) ; ...
        0 ; ...
        0 ; ...
        delta*y(Ji_H) + gamma*y(Ji_H).*y(Ji_Z) - rho*y(Ji_D) ; ...
        0 ; ...
    ];

% Combined right-hand side
%F = @(t, y) F_h (t, y);
%F = @(t, y) F_z (t, y);
F = @(t, y) F_h (t, y) + F_z (t, y) + F_interact (t, y)/dx;

%============================================================
% Run ODE solver

Y0 = [ H0 ; Z0 ; D0 ];
[T, Y] = ode45 (F, T_range, Y0);
H = Y(:, 1:(n+2));
Z = Y(:, (n+2)+1:2*(n+2));
D = Y(:, 2*(n+2)+1:3*(n+2));

%============================================================
% Plots

% Plot initial conditions
figure (1); clf;
plot (J*dx, H0, '-', J*dx, Z0, '-.');
grid on;
legend ('Humans, H_0(x)' ...
        , 'Zombies, Z_0(x)' ...
        , 'Location', 'Best');
title ('Initial population densities');
xlabel ('Position (x)');

% Plot solution over time
figure (2); clf;
pcolor (J*dx, T, H);
shading ('interp');
xlabel ('Position (x)');
ylabel ('Time (t)');
title ('H(x, t): Human population density');
axis square;
colorbar;
cax_h = caxis;

figure (3); clf;
pcolor (J*dx, T, Z);
shading ('interp');
shading ('interp');
xlabel ('Position (x)');
ylabel ('Time (t)');
title ('Z(x, t): Zombie population density');
axis square;
colorbar;
cax_z = caxis;

cax_any = [min([cax_h(1) cax_z(1)]) max([cax_h(2) cax_z(2)])];
figure (2); caxis (cax_any);
figure (3); caxis (cax_any);

% Check conservation of population
figure (4); clf;
plot (T, sum (H, 2)*dx, '-', T, sum (Z, 2)*dx, '-.');
title ('Total populations over time');
xlabel ('Time (t)');
l_id = legend ('Humans, $\sum_j H(x_j,t) \Delta x$' ...
               , ['Zombies, $\sum_j Z(x_j,t) \Delta x$'] ...
               , 'Location', 'Best');
set (l_id, 'Interpreter', 'Latex');
grid on;

% eof
